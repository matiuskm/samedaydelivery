@extends('layouts.error')
@section('title', 'Page Not Found')
@section('heading', 'Page Not Found')
@section('message', 'Sorry, but the page you were trying to view does not exist.')