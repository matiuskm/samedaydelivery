@extends('layouts.error')
@section('title', 'Error')
@section('heading', 'Error: 406')
@section('message', 'Sorry, but the page you were trying to view generates an error.')