@extends('layouts.error')
@section('title', 'Error')
@section('heading', 'Error: 500')
@section('message', 'Sorry, but the page you were trying to view generates an error.')