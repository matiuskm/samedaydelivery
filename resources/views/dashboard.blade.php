@extends('layouts.main')
@section('content')
  <div class="justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
    <h1 class="h2">Dashboard</h1>
    <hr class="colorgraph">
  </div>

  <h2>Pickup Orders</h2>
  @if ($orders)
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#Order ID</th>
          <th>Courier</th>
          <th>Service Type</th>
          <th>Courier Order ID</th>
          <th>Status</th>
          <th>Ordered On</th>
          <th>Last Status Updated On</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($orders as $order)
        <tr>
          <td>{{$order->ft_order_id}}</td>
          <td>{{$order->courier}}</td>
          <td>{{ucfirst($order->service_type)}}</td>
          <td>{{$order->sd_order_id}}</td>
          <td id='st-{{$order->id}}'>{{$order->status}}</td>
          <td>{{$order->created_at}}</td>
          <td id='up-{{$order->id}}'>{{$order->updated_at}}</td>
          <td><a id="refresh" class="btn btn-info btn-sm" rel="{{ $order->id }}"><span data-feather="refresh-cw" aria-hidden="true"></span> Update Status</a></td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  <nav aria-label="Page navigation example">
    {{$orders->links("pagination::bootstrap-4")}}
  </nav>
  @else
    No transactions yet
  @endif
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $(document).on('click', 'a#refresh', function() {
        var id = $(this).attr('rel');
        var data = {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          id,
        };
        $.ajaxSetup({
          headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.post("{{ url('/update') }}", {data: JSON.stringify(data)}, function(data) {
          if (data) {
            var res = JSON.parse(data);
            if (res.code == 200) {
              $('#st-'+id).text(res.data.status);
              $('#up-'+id).text(res.data.update);
            }
          }
        });
      });
      // $(document).on('click', 'a#refresh', function() {
      //   var id = jQuery(this).attr('rel');
      //   console.log(id);
      //   jQuery.get('update',{gkid: id}, function(data) {
      //     jQuery('#st-'+id).text(data.status);
      //     jQuery('#up-'+id).text(data.updated_at);
      //   });
      // });
    });
  </script>
@endsection