<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="The F Thing Sameday Delivery Service">
    <meta name="author" content="Kristian Multz">
    <link rel="shortcut icon" type="image/png" href="{{ secure_asset('/img/favico.png') }}"/>
    <!-- Bootstrap core CSS -->
    <link href="{{secure_asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{secure_asset('css/custom.css')}}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src=\"{{asset("js/jquery.min.js")}}\"><\/script>')</script>
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Sameday Delivery</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a href="{{ route('logout') }}" class="nav-link"
            onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            Signout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        @include('partials.sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
