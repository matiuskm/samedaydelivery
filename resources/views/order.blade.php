@extends('layouts.main')
@section('content')
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      @if (isset($message))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> 
          {{ $message }}
          <span class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </span>
        </div>
      @endif
      @if (isset($error))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
          {{ $error }}
          <span class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </span>
        </div>
      @endif
    </div>
  </div>


  <div class="justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
    <h1 class="h2">Pickup Order <small>and get it done.</small></h1>
    <hr class="colorgraph">
  </div>

  <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">

    <form role="form" method="POST" class="border-bottom" action="{{secure_url('/order')}}">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="tOrderId" class="sr-only">Order Id</label>
        <div class="input-group">
          <input type="text" name="tOrderId" id="tOrderId" class="form-control input-lg" placeholder="Order ID" tabindex="1" aria-describedby="orderIdHelpBlock">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary"><span data-feather="search"></span></button>
          </div>
        </div>
        <small id="orderIdHelpBlock" class="form-text text-muted">Order id that you get from The F Thing website.</small>
      </div>
    </form>

    @if (isset($shipping_method))
    <form role="form" method="POST" action="{{secure_url('/pickup')}}">
      {{ csrf_field() }}
      <div class="row mb-2">
        <div class="col-sm-12 col-md-6 text-center">
          <span class="form-control-static">Order ID</span><h4>#{{$order_id}}</h4>
          <input type="hidden" class="form-control" id="tstoreOrderId" name="tstoreOrderId" value="{{ $order_id }}">
        </div>
        <div class="col-sm-12 col-md-6 text-center">
          <span class="form-control-static">Chosen shipment</span> <h4>
          @if ($shipping_method === "freeshipping_freeshipping")
            Free Shipping
          @elseif ($shipping_method === "fthing_gosend_sameday" )
            Go-Send Sameday
          @elseif ($shipping_method === "fthing_gosend_instant")
            Go-Send Instant
          @elseif ($shipping_method === "fthing_grab_sameday" )
            Grab Express Sameday
          @elseif ($shipping_method === "fthing_grab_instant")
            Grab Express Instant
          @else
            Other
          @endif
          </h4>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 col-md-12">
          <h4>Shipping Address</h4><hr>
          <p class="form-control-static">
            {{ $shipping_data['firstname'] }} {{ $shipping_data['lastname'] }}<br/>
            {{ $shipping_data['street'] }}<br/>
            {{ $shipping_data['districs'] }}, {{ $shipping_data['city'] }}<br/>
            {{ $shipping_data['telephone'] }}
          </p>
          <input type="hidden" class="form-control" id="tdestinationContactName" name="tdestinationContactName" value="{{ $shipping_data['firstname'] }} {{ $shipping_data['lastname'] }}">
          <input type="hidden" class="form-control" id="tdestinationContactPhone" name="tdestinationContactPhone" value="{{ $shipping_data['telephone'] }}">
          <input type="hidden" class="form-control" id="tdestinationLatLong" name="tdestinationLatLong" value="{{ $shipping_data['coordinate'] }}">
          <input type="hidden" class="form-control" id="tdestinationAddress" name="tdestinationAddress" value="{{ $shipping_data['street'] }}, {{ $shipping_data['districs'] }}">        
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <textarea class="form-control" id="notes" name="notes" rows="3" aria-describedby="notesHelpBlock"></textarea>
            <small id="notesHelpBlock" class="form-text text-muted">Enter notes for the driver.</small>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 col-md-9">
          <div class="form-group">
            <select class="custom-select my-1 mr-sm-2" name="serviceType">
              <option selected>Choose one service type</option>
              <option value="gosend-Instant">Go-Send Instant Delivery</option>
              <option value="gosend-SameDay">Go-Send Same Day Delivery</option>
              <option value="grab-INSTANT">Grab Express Instant Delivery</option>
              <option value="grab-SAME_DAY">Grab Express Same Day Delivery</option>
            </select>
          </div>
        </div>
        <div class="col-sm-12 col-md-3">
          <button class="btn btn-primary mb-2">Order Pickup</button>
        </div>
      </div>
    </form>
    @endif
  </div>

@endsection