<?php

namespace Modules;

use App\RequestLog;
use App\Order;

class GoSend {
  
  // booking url
  const BOOKING_URL_DEV = 'https://integration-kilat-api.gojekapi.com/gojek/booking/v3/makeBooking';
  const BOOKING_URL_PROD = 'https://kilat-api.gojekapi.com/gojek/booking/v3/makeBooking';

  // status url
  const STATUS_URL_DEV = 'https://integration-kilat-api.gojekapi.com/gojek/v2/booking/orderno/';
  const STATUS_URL_PROD = 'https://kilat-api.gojekapi.com/gojek/v2/booking/orderno/';

  // header
  const HEADER_DEV = [
    'Client-ID' => 'xxxxx',
    'Pass-Key' => 'xxxx',
    'Content-Type' => 'application/json'
  ];
  const HEADER_PROD = [
    'Client-ID' => 'xxxx',
    'Pass-Key' => 'xxxxx',
    'Content-Type' => 'application/json'
  ];

  public function order($request) {
    $orderId = $request['tstoreOrderId'];
    $client = new \GuzzleHttp\Client();
    $req = $this->buildOrder($request);
    //dd($req);
    $param = [
      'headers' => $request['env'] === 'dev' ? self::HEADER_DEV : self::HEADER_PROD,
      'json' => $req,
      'verify'=>false
    ];

    $log = new RequestLog();

    try {
      $res = $client->request('POST', $request['env'] === 'dev' ? self::BOOKING_URL_DEV : self::BOOKING_URL_PROD, $param);

      $log->request = json_encode($req);
      $log->response = $res->getBody();

      $resp = json_decode($res->getBody(), true);
      $order = new Order();
      $order->ft_order_id = $orderId;
      $order->sd_order_id = $resp['orderNo'];
      $order->courier = 'GoSend';
      $order->service_type = explode('-', $request['serviceType'])[1];
      $order->save();
      $result = [
        'code' => 200,
        'message' => "Pickup order for {$orderId} has been made with courier number {$resp['orderNo']}"
      ];
    } catch (Exception $e) {
      $log->request = json_encode($req);
      $log->response = $e->getMessage();

      $result = [
        'code' => 400,
        'message' => "There's an error. Please check log."
      ];
    }

    $log->save();
    return json_encode($result);
    //dd($hasil);
  }

  public function status($request) {
    $client = new \GuzzleHttp\Client();
    //dd($req);
    $param = [
      'headers' => $request['env'] === 'dev' ? self::HEADER_DEV : self::HEADER_PROD,
      'verify'=>false
    ];

    $log = new RequestLog();

    try {
      $url = $request['env'] === 'dev' ? self::STATUS_URL_DEV : self::STATUS_URL_PROD;
      $res = $client->request('GET', $url.$request['order']->sd_order_id, $param);

      $log->request = $url;
      $log->response = $res->getBody();

      $resp = json_decode($res->getBody(), true);
      // $order = Order::where('sd_order_id', request('sdid'))->first();
      $order = $request['order'];
      $order->status = $resp['status'];
      $order->save();
      $result = [
        'code' => 200,
        'message' => "Status updated successfully.",
        'data' => [
          'status' => $order->status,
          'update' => \Carbon\Carbon::parse($order->updated_at)->setTimezone('Asia/Jakarta')->format('Y-m-d H:m:s'),
        ]
      ];
    } catch (Exception $e) {
      $log->request = json_encode($req);
      $log->response = $e->getMessage();

      $result = [
        'code' => 400,
        'message' => "Failed to update status."
      ];
    }

    $log->save();
    return json_encode($result);
  }

  private function buildOrder($req) {
    $gosend = array(
      "paymentType" => 3,
      "deviceToken" => "",
      "collection_location" => "pickup",
      "shipment_method" => explode('-', $req['serviceType'])[1],
      "routes" => [
        array(
          "originName" => "",
          "originNote"=> "",
          "originContactName"=> $req['origin']['name'],
          "originContactPhone"=> $req['origin']['phone'],
          "originLatLong"=> $req['origin']['coordinate'],
          "originAddress"=> $req['origin']['address'],
          "destinationName"=> "",
          "destinationNote"=> $req['notes'],
          "destinationContactName"=> $req['tdestinationContactName'],
          "destinationContactPhone"=> $req['tdestinationContactPhone'],
          "destinationLatLong"=> $req['tdestinationLatLong'],
          "destinationAddress"=> $req['tdestinationAddress'],
          "item"=> "Clothing",
          "storeOrderId"=> $req['tstoreOrderId'],
          "insuranceDetails"=>
          array(
            "applied"=> "false"
          )
        )
      ]
    );
    return $gosend;
  }
}