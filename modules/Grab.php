<?php

namespace Modules;

use App\RequestLog;
use App\Order;

class Grab {

  // url endpoint grab
  const URL_DEV = 'https://api.stg-myteksi.com';
  const URL_PROD = 'https://api.grab.com';

  // credentials
  const SAFE_ID_DEV = "xxxxxxxxxx";
  const SECRET_DEV = "xxxxxxxx";
  const SAFE_ID_PROD = "";
  const SECRET_PROD = "";

  // miscelaneous
  const CONTENT_TYPE_HEADER = "Content-Type: ";
  const AUTHORIZATION_HEADER = "Authorization: ";
  const DATE_HEADER = "Date: ";
  const CONTENT_LENGTH_HEADER = "Content-Length: ";

  private function composeBody($body) {
    // print "===== START BODY ====\n";

    $packages = [
      array(
        "name" => "Clothing",
        "description" => "Clothing and Accessories",
        "quantity" => 1,
        "price" => 0,
        "insuranceValue" => 0,
        "insuranceType" => "BASIC",
        "currency" => array(
          "code" => "IDR",
          "symbol" => "Rp",
          "exponent" => 0
        ),
        "dimensions" => array (
          "height" => 0,
          "width" => 0,
          "depth" => 0,
          "weight" => 0,
        ),
      )
    ];

    $sender = array (
      "firstName" => $body['origin']['name'],
      "lastName" => "",
      "title" => "",
      "companyName" => "",
      "email" => "",
      "phone" => $body['origin']['phone'],
      "smsEnabled" => true,
      "instruction" => ""
    );

    $recipient = array (
      "firstName" => $body['tdestinationContactName'],
      "lastName" => "",
      "title" => "",
      "companyName" => "",
      "email" => "",
      "phone" => $body['tdestinationContactPhone'],
      "smsEnabled" => true,
      "instruction" => $body['notes']
    );

    $origin = array(
      "address" => $body['origin']['address'],
      "keywords" => $body['origin']['name'],
      "coordinates" => array(
        "latitude" => floatval($body['origin']['lat']),
        "longitude" => floatval($body['origin']['long']),
      ),
      "extra" => array()
    );

    $destination = array(
      "address" => $body['tdestinationAddress'],
      "keywords" => "",
      "coordinates" => array(
        "latitude" => floatval(explode(",",$body['tdestinationLatLong'])[0]),
        "longitude" => floatval(explode(",",$body['tdestinationLatLong'])[1])
      ),
      "extra" => array()
    );

    // combine all of the parameters into one single array
    $reqArray = array(
      "merchantOrderId" => $body['tstoreOrderId'],
      "serviceType" => explode('-', $body['serviceType'])[1],
      "packages" => $packages,
      "sender" => $sender,
      "recipient" => $recipient,
      "origin" => $origin,
      "destination" => $destination
    );
    
    $reqBody = json_encode($reqArray);
    // print $reqBody."\n";
    // print "===== END BODY =====\n";
    return $reqBody;
  }

  private function getHeaders($env, $method, $path, $body, $mime) {
    $headers = array();

    // Content-Type
    if (strtoupper($method) != "GET") {
      array_push($headers,self::CONTENT_TYPE_HEADER.$mime);
      // print self::CONTENT_TYPE_HEADER.$mime."\n";
    }

    // Date
    $gmtdate = gmdate("D, d M Y H:i:s", time())." GMT";
    array_push($headers,self::DATE_HEADER.$gmtdate);
    // print self::DATE_HEADER.$gmtdate."\n";

    // Authorization
    $hmacDigest = $this->createHmacDigest($env, $method, $mime, $path, $body, $gmtdate);
    $auth = self::SAFE_ID_DEV.":".$hmacDigest;
    array_push($headers,self::AUTHORIZATION_HEADER.$auth);
    // print self::AUTHORIZATION_HEADER.$auth."\n";

    array_push($headers,self::CONTENT_LENGTH_HEADER.strlen($body));
    // print self::CONTENT_LENGTH_HEADER.strlen($body)."\n";

    // print "Headers to be sent: ".print_r($headers, true)."\n";

    return $headers;
  }

  private function hashHmac($text, $secret) {
    try {
      return base64_encode(hash_hmac("sha256", $text, $secret, true));
    } catch (Exception $e) {
    // print "Exception: ".$e->getMessage()."\n";
      return $text;
    }
  }

  private function hashContent($text) {
    try {
      return base64_encode(hash("sha256", $text, true));
    } catch (Exception $e) {
      // print "Exception: ".$e->getMessage()."\n";
      return $text;
    }
  }

  private function createContentDigest($body) {
    if (!is_null($body)) {
      if (!empty($body)) {
        return $this->hashContent($body);
      }
    }
    // print "No hash needed for content digest"."\n";
    return "";
  }

  private function createHmacDigest($env, $method, $contentType, $path, $body, $date) {
    $result = "";
    
    // method
    $result .= strtoupper($method)."\n";

    // content-type
    if (strtoupper($method) == "GET") $contentType = "";
    $result .= $contentType."\n";

    // date
    $result .= $date."\n";

    // path
    $result .= $path."\n";

    // content-digest
    $result .= $this->createContentDigest($body)."\n";

    // print $result."\n";
    return $this->hashHmac($result, $env === 'dev' ? self::SECRET_DEV : self::SECRET_PROD);
  }

  public function delivery($request) {
    $orderId = $request['tstoreOrderId'];
    $log = new RequestLog();

    try {
      $method = "POST";
      $mime   = "application/json";
      
      $body   = $this->composeBody($request);
      // dd($body);
      $main_url = $request['env'] === 'dev' ? self::URL_DEV : self::URL_PROD;
      $apiContext = '/v1/deliveries';
      $url = $main_url.$apiContext;
      // print 'Connecting to: '.$url."\n";
      $headers= $this->getHeaders($request['env'], $method, $apiContext, $body, $mime);
      array_push($headers,'Content-Length: '.strlen($body));

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
              $body);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $res = curl_exec ($ch);
      // print 'HTTP Code: '.curl_getinfo($ch, CURLINFO_HTTP_CODE)."\n";
      // print 'response: '.print_r($response)."\n";

      $log->request = $body;
      $log->response = $res;
      curl_close ($ch);       
      $resp = json_decode($res, true);
      $order = new Order();
      $order->ft_order_id = $orderId;
      $order->sd_order_id = $resp['deliveryID'];
      $order->courier = 'Grab Express';
      $order->service_type = explode('-', $request['serviceType'])[1];
      $order->save();

      $result = [
        'code' => 200,
        'message' => "Pickup order for {$orderId} has been made with courier number {$resp['deliveryID']}"
      ];
    } catch (Exception $e) {
      $log->request = json_encode($req);
      $log->response = $e->getMessage();

      $result = [
        'code' => 400,
        'message' => "There's an error. Please check log."
      ];
    }

    $log->save();
    return json_encode($result);
  }

  public function status($request) {
    $orderId = $request['order']->sd_order_id;
    $log = new RequestLog();

    try {
      $method = "GET";
      $mime   = "";      
      $body   = "";
      // dd($body);
      $main_url = $request['env'] === 'dev' ? self::URL_DEV : self::URL_PROD;
      $apiContext = '/v1/deliveries/'.$request['order']->sd_order_id;
      $url = $main_url.$apiContext;
      // print 'Connecting to: '.$url."\n";
      $headers= $this->getHeaders($request['env'], $method, $apiContext, $body, $mime);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $res = curl_exec ($ch);
      // print 'HTTP Code: '.curl_getinfo($ch, CURLINFO_HTTP_CODE)."\n";
      // print 'response: '.print_r($response)."\n";

      $log->request = $url;
      $log->response = $res;
      curl_close ($ch);
      $resp = json_decode($res, true);
      // dd($resp);
      $order = $request['order'];
      $order->status = $resp['status'];
      $order->save();
      $result = [
        'code' => 200,
        'message' => "Status updated successfully.",
        'data' => [
          'status' => $order->status,
          'update' => \Carbon\Carbon::parse($order->updated_at)->setTimezone('Asia/Jakarta')->format('Y-m-d H:m:s'),
        ]
      ];
    } catch (Exception $e) {
      $log->request = json_encode($req);
      $log->response = $e->getMessage();

      $result = [
        'code' => 400,
        'message' => "There's an error. Please check log."
      ];
    }

    $log->save();
    return json_encode($result);

  }
}