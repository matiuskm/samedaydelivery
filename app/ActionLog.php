<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionLog extends Model {
	protected $table = "action_logs";
	protected $fillable = array('userid', 'action');
}
