<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
	protected $table = "orders";
	protected $fillable = array('ft_order_id','sd_order_id','courier','service_type','status');
}
