<?php

  namespace App\Http\Controllers;

  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Auth;
  use App\Order;
  use App\ActionLog;
  use Modules\GoSend;
  use Modules\Grab;

  class SamedayController extends Controller {

    // global
    const ENV = 'dev'; // 'dev', 'prod'
    const ORIGIN_SHIPPER = 'The F Thing';
    const ORIGIN_PHONE = '08111138080';
    const ORIGIN_COORDINATE = '-6.184970,106.831740';
    const ORIGIN_ADDRESS = 'MNC News Center lt. 12, Jalan Wahid Hasyim 28, Menteng, 10340';
    
    // magento api url
    const MAGE_API_DEV = 'https://dev.thefthing.com/app_mobile/';
    const MAGE_API_PROD = 'https://thefthing.com/app_mobile/';

    // allowed shipping method & region
    const ALLOWED_SHIPPING_METHOD = [
      'freeshipping_freeshipping',
      'fthing_gosend_sameday',
      'fthing_gosend_instant',
      'fthing_grab_instant',
      'fthing_grab_sameday',
    ];
    const ALLOWED_REGION = 'DKI JAKARTA';

    public function __construct() {
      $this->middleware('auth');
    }

    public function index() {
      $orders = Order::orderBy('created_at', 'desc')->paginate(5);
      // dd($orders);
      return view('dashboard')->with('orders', $orders);
    }

    public function loadOrderFromMagento() {
      if (!request()->has('tOrderId')) return view('order');
      $actionLog = new ActionLog;
      $actionLog->userid = Auth::id();
      $actionLog->action = "retrieve order id #".request('tOrderId');
      $actionLog->save();

      $client = new \GuzzleHttp\Client();
      $params = [
        'form_params' => [
          'order-id'=>request('tOrderId')
        ], 
        'verify'=>false
      ];
      $url = self::ENV === 'dev' ? self::MAGE_API_DEV : self::MAGE_API_PROD;
      $req = $client->request('POST', $url."checkout/checkOrderByIncrementId.php", $params);
      $res = json_decode($req->getBody(), true);

      $data = $res['result'];
      if ($res['status']['code'] == 200) {
        if (in_array($data['shipping_method'], self::ALLOWED_SHIPPING_METHOD) & strtoupper($data['shipping_data']['region']) === strtoupper(self::ALLOWED_REGION)) {
          $data['message'] = 'Ready to order for pickup';
        } else {
          $data['error'] = 'This order is not eligible for sameday delivery';
        }
      } else {
        $data['error'] = 'Order id doesn\'t exist';
      }

      return view('order', $data);
    }

    public function pickup() {
      $userLog = new ActionLog;
      $userLog->userid = Auth::id();
      $userLog->action = "request pickup for order ".request('tstoreOrderId');
      $userLog->save();

      $service = request('serviceType');
      if (isset($service) && !empty($service)) {
        $request = request()->all();
        if (strpos($service, 'gosend') !== false) {
          // proceed with gosend
          $gosend = new GoSend();
          $request['env'] = self::ENV;
          $request['origin'] = [
            'name' => self::ORIGIN_SHIPPER,
            'coordinate' => self::ORIGIN_COORDINATE,
            'address' => self::ORIGIN_ADDRESS,
            'phone' => self::ORIGIN_PHONE
          ];
          $resp = $gosend->order($request);
        } else if (strpos($service, 'grab') !== false) {
          // proceed with grab
          $grab = new Grab();
          $request['env'] = self::ENV;
          $request['origin'] = [
            'name' => self::ORIGIN_SHIPPER,
            'lat' => explode(",",self::ORIGIN_COORDINATE)[0],
            'long' => explode(",",self::ORIGIN_COORDINATE)[1],
            'address' => self::ORIGIN_ADDRESS,
            'phone' => self::ORIGIN_PHONE
          ];
          $resp = $grab->delivery($request);
        }

        $resp = json_decode($resp, true);
        $data = [];
        if ($resp['code'] === 200) {
          $data['message'] = $resp['message'];
        } else {
          $data['error'] = $resp['message'];
        }

        return view('order', $data);
      }
    }

    public function update() {
      $data = json_decode(request('data'));
      $order = Order::where('id', '=', $data->id)->first();
      // dd($order);
      if ($order) {
        $userLog = new ActionLog;
        $userLog->userid = Auth::id();
        $userLog->action = "update courier status for ".$order->sd_order_id;
        $userLog->save();

        $request['env'] = self::ENV;
        $request['order'] = $order;

        if ($order->courier === 'GoSend') {
          $gosend = new GoSend();
          echo $gosend->status($request);
        } else if ($order->courier === 'Grab Express') {
          $grab = new Grab();
          echo $grab->status($request);
        }
        
      }
    }
  }